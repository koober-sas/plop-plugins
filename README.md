# Plop Plugin Collection

## Packages

<!-- AUTO-GENERATED-CONTENT:START (SUBPACKAGELIST:verbose=true) -->
* [@koober/plop-doc](packages/plop-doc) - PlopJS plugin for documentation
* [@koober/plop-plugin](packages/plop-plugin) - PlopJS generic plugin interface
* [@koober/plop-react-native-appicon](packages/plop-react-native-appicon) - PlopJS plugin for generating app icon
<!-- AUTO-GENERATED-CONTENT:END -->

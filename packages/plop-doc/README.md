<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=# Plop Documentation plugin _(${name})_) -->
# Plop Documentation plugin _(@koober/plop-doc)_
<!-- AUTO-GENERATED-CONTENT:END -->

[![NPM Version][package-version-svg]][package-url]
[![License][license-image]][license-url]

<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=> ${description}&unknownTxt= ) -->
> PlopJS plugin for documentation
<!-- AUTO-GENERATED-CONTENT:END -->

## Installation

<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=```console\nnpm install --save-dev ${name}\n```) -->
```console
npm install --save-dev @koober/plop-doc
```
<!-- AUTO-GENERATED-CONTENT:END -->

## Usage

In the `plopfile.js` of your project

<!-- AUTO-GENERATED-CONTENT:START (CODE:src=./doc/usage.js) -->
<!-- The below code snippet is automatically added from ./doc/usage.js -->
```js
// @ts-check
const { PlopPlugin } = require('@koober/plop-plugin');
const { default: PlopDocument, defaultTemplates } = require('@koober/plop-doc');

module.exports = (
  /** @type {import('@koober/plop-plugin').PlopModule} */
  plop
) => {
  // apply
  PlopPlugin.apply(
    plop,
    // Combine all plugins
    PlopPlugin.compose(
      // ...other plugins
      PlopDocument(plop, {
        templates: defaultTemplates,
      })
      // ...other plugins
    )
  );
};
```
<!-- AUTO-GENERATED-CONTENT:END -->

## License
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[${license}][license-url] © ${author}) -->
[MIT][license-url] © Julien Polo <julien.polo@koober.com>
<!-- AUTO-GENERATED-CONTENT:END -->

<!-- VARIABLES -->

<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[package-version-svg]: https://img.shields.io/npm/v/${name}.svg?style=flat-square) -->
[package-version-svg]: https://img.shields.io/npm/v/@koober/plop-doc.svg?style=flat-square
<!-- AUTO-GENERATED-CONTENT:END -->
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[package-url]: https://www.npmjs.com/package/${name}) -->
[package-url]: https://www.npmjs.com/package/@koober/plop-doc
<!-- AUTO-GENERATED-CONTENT:END -->
<!-- AUTO-GENERATED-CONTENT:START (PKGJSON:template=[license-image]: https://img.shields.io/badge/license-${license}-green.svg?style=flat-square) -->
[license-image]: https://img.shields.io/badge/license-MIT-green.svg?style=flat-square
<!-- AUTO-GENERATED-CONTENT:END -->
[license-url]: ../../LICENSE

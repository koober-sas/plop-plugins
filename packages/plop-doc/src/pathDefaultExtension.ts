import * as path from 'path';

/**
 * Append default extension if file does not have
 *
 * @param pathString - the path to modify
 * @param defaultExtension - the default extension if not found
 */
export default function pathDefaultExtension(pathString: string, defaultExtension: string): string {
  const hasExtension = path.extname(pathString).length > 0;

  return hasExtension ? pathString : pathString + defaultExtension;
}

import pathDefaultExtension from './pathDefaultExtension';

describe('pathDefaultExtension()', () => {
  test('should return identity if has already an extension', () => {
    expect(pathDefaultExtension('toto.gif', '.gif')).toEqual('toto.gif');
  });
  test('should add extension if not present', () => {
    expect(pathDefaultExtension('toto', '.gif')).toEqual('toto.gif');
  });
});

import { defaultTemplates, getTemplatePath } from './template';

describe('defaultTemplate', () => {
  test('should have keys', () => {
    expect(defaultTemplates).toEqual(
      expect.objectContaining({
        'README.md': expect.any(Object),
        'ADR.md': expect.any(Object),
        'TASK.todo': expect.any(Object),
      })
    );
  });
});

describe('getTemplatePath()', () => {
  test('should return a correct path', () => {
    // eslint-disable-next-line id-match
    expect(getTemplatePath()).toEqual(__dirname.replace('/src', '/template'));
  });
});

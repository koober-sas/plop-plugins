/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { PlopModule } from '@koober/plop-plugin';
import { TemplateSet, Template } from './template';
import pathDefaultExtension from './pathDefaultExtension';
import { DocAction } from './action';

export interface DocGeneratorConfig {
  templates: TemplateSet.Type;
}

/**
 * Doc generator factory
 *
 * @param config
 */
export function DocGenerator(plop: PlopModule, { templates = TemplateSet.empty }: DocGeneratorConfig) {
  const templateEntry = (userData: { templateName: string }) =>
    TemplateSet.findByName(templates, userData.templateName);
  const hasNoProperty = (property: keyof Template.Type) => (userData: { templateName: string }) => {
    const value = templateEntry(userData)[property];

    return value == null;
  };
  const required =
    (field: keyof Template.Type['required']) =>
    (userData: { templateName: string }): boolean =>
      Boolean(templateEntry(userData).required[field]);

  return {
    description: 'Documentation file',
    prompts: [
      {
        type: 'list',
        name: 'templateName',
        message: 'Select type of document',
        choices: TemplateSet.names(templates),
      },
      {
        type: 'input',
        name: 'targetDirectory',
        message: 'Enter target directory',
        when: hasNoProperty('targetDirectory'),
      },
      {
        type: 'input',
        name: 'targetFileName',
        message: 'Enter new file name',
        when: hasNoProperty('targetFileName'),
        filter(answer: string, userData: { templateName: string }) {
          return pathDefaultExtension(answer, Template.extname(templateEntry(userData)));
        },
      },
      {
        type: 'input',
        name: 'documentTitle',
        message: 'Enter document title',
        when: required('documentTitle'),
      },
    ],
    actions: (userData: {
      templateName: string;
      targetDirectory: string | null | undefined;
      targetFileName: string | null | undefined;
      documentTitle: string | null | undefined;
    }) => {
      return [
        DocAction.addFile({
          template: templateEntry(userData),
          targetDirectory: userData.targetDirectory,
          targetFileName: userData.targetFileName,
        }),
      ];
    },
  };
}

import { DocAction } from './action';
import { Template } from './template';

describe('Action', () => {
  describe('.addFile()', () => {
    const anyTemplate = Template.create({
      targetDirectory: 'dir',
      targetFileName: 'test.test',
      templateFile: 'template.hbs',
    });

    test('should use templateFile from template', () => {
      expect(
        DocAction.addFile({
          targetDirectory: undefined,
          targetFileName: undefined,
          template: anyTemplate,
        })
      ).toEqual(
        expect.objectContaining({
          templateFile: 'template.hbs',
        })
      );
    });
    test('should use targetDirectory and targetFileName from template if not defined', () => {
      expect(
        DocAction.addFile({
          targetDirectory: undefined,
          targetFileName: undefined,
          template: anyTemplate,
        })
      ).toEqual(
        expect.objectContaining({
          data: expect.objectContaining({
            dirname: anyTemplate.targetDirectory,
            filename: anyTemplate.targetFileName,
          }),
          path: `${anyTemplate.targetDirectory}/${anyTemplate.targetFileName}`,
        })
      );
    });
    test('should use targetDirectory if defined', () => {
      expect(
        DocAction.addFile({
          targetDirectory: 'overrideDir',
          targetFileName: undefined,
          template: anyTemplate,
        })
      ).toEqual(
        expect.objectContaining({
          data: expect.objectContaining({
            dirname: 'overrideDir',
            filename: anyTemplate.targetFileName,
          }),
          path: `overrideDir/${anyTemplate.targetFileName}`,
        })
      );
    });
    test('should use targetFileName if defined', () => {
      expect(
        DocAction.addFile({
          targetDirectory: undefined,
          targetFileName: 'overrideFile',
          template: anyTemplate,
        })
      ).toEqual(
        expect.objectContaining({
          data: expect.objectContaining({
            dirname: anyTemplate.targetDirectory,
            filename: 'overrideFile',
          }),
          path: `${anyTemplate.targetDirectory}/overrideFile`,
        })
      );
    });
    test('should add now to data', () => {
      jest.spyOn(Date, 'now').mockReturnValue(3);

      expect(
        DocAction.addFile({
          targetDirectory: undefined,
          targetFileName: undefined,
          template: anyTemplate,
        })
      ).toEqual(
        expect.objectContaining({
          data: expect.objectContaining({
            now: Date.now(),
          }),
        })
      );
    });
  });
});

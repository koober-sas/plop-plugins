/* cSpell:ignore lightgrey */
export namespace DocHelper {
  /**
   * Return a formatted date
   *
   * @param timestampOrDate - number or date or string
   */
  export function date(timestampOrDate: number | string | Date): string {
    const [dateString] = new Date(timestampOrDate).toISOString().split('T');

    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return dateString!;
  }

  /**
   * Generate markdown status badge
   *
   * @param status - status code name
   */
  export function statusBadge(status: 'approved' | 'rejected' | 'deprecated' | 'draft'): string {
    const statusCode = status.toLowerCase() as typeof status;
    const color = (
      {
        approved: 'green',
        rejected: 'orange',
        deprecated: 'lightgrey',
        draft: 'blue',
      } as const
    )[statusCode]; // ?? ('lightgrey' as const);

    return `![Status: ${status}](https://img.shields.io/badge/status-${statusCode}-${color}.svg)`;
  }
}

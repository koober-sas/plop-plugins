import * as path from 'path';
import { Template } from './template';

export namespace DocAction {
  /**
   * Create new file
   *
   * It will pass template data:
   * - dirname : generated file directory
   * - filename : generated file name
   * - now : current timestamp
   *
   * @param options
   *  - template : the Template.Type configuration
   *  - data: additional data available in template
   *  - targetDirectory (optional): override target directory
   *  - targetFileName (optional): override target file name
   */
  export function addFile<Data extends { dirname?: string; filename?: string; now?: number }>({
    template,
    targetDirectory,
    targetFileName,
    ...rest
  }: {
    template: Template.Type;
    data?: Data;
    targetDirectory: string | null | undefined;
    targetFileName: string | null | undefined;
  }): {
    data: {
      dirname: string;
      filename: string;
      now: number;
    } & Omit<Data, 'filename' | 'dirname' | 'now'>;
    path: string;
    templateFile: string;
    type: 'add';
  } {
    const dirname = targetDirectory ?? template.targetDirectory ?? '';
    const filename = targetFileName ?? template.targetFileName ?? '';

    return {
      // @ts-ignore cannot fix that error
      data: {
        dirname,
        filename,
        now: Date.now(),
        ...rest.data,
      },
      path: path.join(dirname, filename),
      templateFile: template.templateFile,
      type: 'add',
    };
  }
}

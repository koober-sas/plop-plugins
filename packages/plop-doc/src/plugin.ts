/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { PlopModule, PlopPlugin } from '@koober/plop-plugin';
import { DocHelper } from './helper';
import { DocGeneratorConfig, DocGenerator } from './generator';

/**
 * @param plop
 * @param config
 *  - templates - A dictionary of
 *   {
 *     [string]: {
 *       // The template file path to use
 *       templateFile: string,
 *
 *       // The target file name for generated file
 *       targetFileName?: string,
 *
 *       // The target file directory for generated file
 *       targetDirectory?: string
 *     }
 *   }
 */
export function DocPlugin(plop: PlopModule, config: DocGeneratorConfig) {
  /**
   * Plop middleware that will return a new structure with new generators/helpers
   */
  return PlopPlugin.merge({
    generators: {
      doc: DocGenerator(plop, config),
    },
    helpers: DocHelper,
  });
}

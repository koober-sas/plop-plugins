import { createPlopMock } from '@koober/plop-plugin/lib/testing';
import { DocPlugin } from './plugin';

describe('DocPlugin()', () => {
  const anyPlop = createPlopMock();

  test('should add helpers and generators to input', () => {
    expect(DocPlugin(anyPlop, { templates: {} })({ generators: {}, helpers: {} })).toEqual({
      generators: {
        doc: expect.any(Object),
      },
      helpers: expect.objectContaining({
        date: expect.any(Function),
      }),
    });
  });
});

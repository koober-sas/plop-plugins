import * as path from 'path';

/**
 * Return the template path location
 */
export function getTemplatePath(): string {
  return path.join(path.dirname(__dirname), 'template');
}

export namespace Template {
  export interface Type {
    /**
     * Template to use to generate the file
     */
    templateFile: string;

    /**
     * Name of generated file
     */
    targetFileName?: string;

    /**
     * Directory where the file will be generated
     */
    targetDirectory?: string;

    /**
     * Required fields
     */
    required: {
      /**
       * The document title (ex: Improve)
       */
      documentTitle?: boolean;
    };
  }

  /**
   * Return a new template descriptor
   *
   * @param t
   */
  export function create({
    templateFile,
    required = {},
    ...optional
  }: {
    templateFile: Type['templateFile'];
    targetFileName?: Type['targetFileName'];
    targetDirectory?: Type['targetDirectory'];
    required?: Type['required'];
  }): Readonly<Type> {
    return Object.freeze({ templateFile, required, ...optional });
  }

  /**
   * Return the extension name of the template file without default .hbs if present
   *
   * @param template
   */
  export function extname(template: Type): string {
    const baseNameWithoutExtension = path.basename(template.templateFile, '.hbs');

    return path.extname(baseNameWithoutExtension);
  }
}

export namespace TemplateSet {
  export type Type = Record<string, Template.Type>;

  export const empty = Object.freeze({});

  export function create<T extends Type>(data: T): Readonly<T> {
    return Object.freeze({ ...data });
  }

  export function names<T extends Type>(templateSet: T): Array<keyof T> {
    return Object.keys(templateSet);
  }

  export function findByName(templateSet: Type, templateName: string): Template.Type {
    const templateEntry = templateSet[templateName];
    if (templateEntry == null) {
      throw new Error(`${templateName} must be an enum value of "${TemplateSet.names(templateSet).join('"|"')}"`);
    }

    return templateEntry;
  }
}

/**
 * Dictionary of all default templates
 */
export const defaultTemplates = TemplateSet.create({
  'README.md': Template.create({
    targetDirectory: '',
    targetFileName: 'README.md',
    templateFile: path.join(getTemplatePath(), 'README.md.hbs'),
    required: {},
  }),
  'ADR.md': Template.create({
    targetDirectory: path.join('docs', 'adr'),
    targetFileName: '{{date now}}--{{dashCase documentTitle}}.md',
    templateFile: path.join(getTemplatePath(), 'ADR.md.hbs'),
    required: {
      documentTitle: true,
    },
  }),
  'TASK.todo': Template.create({
    targetDirectory: path.join('docs', 'todo'),
    targetFileName: '{{dashCase documentTitle}}.md',
    templateFile: path.join(getTemplatePath(), 'TASK.todo.hbs'),
    required: {
      documentTitle: true,
    },
  }),
});

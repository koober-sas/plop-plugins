// @ts-check
const { PlopPlugin } = require('@koober/plop-plugin');
const { default: PlopDocument, defaultTemplates } = require('@koober/plop-doc');

module.exports = (
  /** @type {import('@koober/plop-plugin').PlopModule} */
  plop
) => {
  // apply
  PlopPlugin.apply(
    plop,
    // Combine all plugins
    PlopPlugin.compose(
      // ...other plugins
      PlopDocument(plop, {
        templates: defaultTemplates,
      })
      // ...other plugins
    )
  );
};

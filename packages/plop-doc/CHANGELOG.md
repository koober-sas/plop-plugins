# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.4](https://gitlab.com/koober-sas/plop-plugins/compare/@koober/plop-doc@1.0.3...@koober/plop-doc@1.0.4) (2021-02-24)


### Bug Fixes

* **lint:** use conventional naming ([96c1b20](https://gitlab.com/koober-sas/plop-plugins/commit/96c1b2012257652c282ac5aae565b5eed6ca5216))

import * as path from 'path';

const templatePath = path.join(path.dirname(__dirname), 'template');

function templateFile(...args: Array<string>): string {
  return path.join(templatePath, ...args);
}

/**
 * Return the template path location
 */
export function getTemplatePath(): string {
  return templatePath;
}

/**
 * Frozen object that describes all available templates
 */
export const defaultTemplates = Object.freeze({
  android: Object.freeze({
    'colors.xml': templateFile('android', 'colors.xml.hbs'),
    // eslint-disable-next-line @typescript-eslint/naming-convention
    'launch_screen_bitmap.xml': templateFile('android', 'launch_screen_bitmap.xml.hbs'),
    'styles.xml': templateFile('android', 'styles.xml.hbs'),
  }),
  ios: Object.freeze({
    'AppIconSetContents.json': templateFile('ios', 'AppIconSetContents.json.hbs'),
    'LaunchImageContents.json': templateFile('ios', 'LaunchImageContents.json.hbs'),
  }),
});

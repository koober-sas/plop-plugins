import gm from './graphicMagick';

export function readPixelColor(imagePath: string, pixelX: number, pixelY: number): Promise<string> {
  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  return new Promise((resolve, reject) => {
    gm(imagePath)
      .crop(pixelX, pixelY)
      .identify('%[hex:s]', (error, imageMagickColor) => {
        if (error != null) return reject(error);

        return resolve(`#${imageMagickColor}`);
      });
  });
}

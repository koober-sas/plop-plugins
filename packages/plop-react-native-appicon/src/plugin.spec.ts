/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { PlopPlugin } from '@koober/plop-plugin';
import { createPlopMock } from '@koober/plop-plugin/lib/testing';
import { RNAppIconPlugin } from './plugin';

describe('RNAppIconPlugin()', () => {
  const anyPlop = createPlopMock();

  test('should add helpers and generators to input', () => {
    expect(
      RNAppIconPlugin(anyPlop, {
        projectName: 'foobar',
      })(PlopPlugin.empty)
    ).toEqual({
      generators: {
        'react-native-appicon': expect.any(Object),
      },
      helpers: {},
    });
  });
});

import * as GraphicMagickDefault from 'gm';

/**
 * Alias to graphic magic with image magick backend
 */
export const graphicMagick = GraphicMagickDefault.subClass({ imageMagick: true });

export default graphicMagick;

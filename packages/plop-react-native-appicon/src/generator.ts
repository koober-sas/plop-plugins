/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* cSpell:ignore ASSETCATALOG,LAUNCHIMAGE */
import * as path from 'path';
import { PlopModule } from '@koober/plop-plugin';
import * as iconGenerator from './iconGenerator';
import { defaultTemplates } from './template';

namespace Maybe {
  export type Nothing = null | undefined;
  export type Just<V> = Exclude<V, Nothing>;
  export type Type<V> = Nothing | Just<V>;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  export function isNothing(anyValue: any): anyValue is Nothing {
    return anyValue === undefined || anyValue === null;
  }

  export function map<T, U>(maybe: Type<T>, mapFn: (t: T) => Just<U>): Type<U> {
    return isNothing(maybe) ? maybe : mapFn(maybe);
  }

  export function orElse<T, U>(maybe: Type<T>, otherValue: U): T | U {
    return isNothing(maybe) ? otherValue : maybe;
  }
}

interface AssetConfig {
  appIcon?: string;
  launchImage?: string;
  launchImageBackgroundColor?: string;
}
type IOSConfig = AssetConfig;
type AndroidConfig = AssetConfig;

export interface RNAppIconConfig {
  projectName: string;
  default?: AssetConfig;
  ios?: IOSConfig;
  android?: AndroidConfig;
}

export function RNAppIconGenerator(plop: PlopModule, config: RNAppIconConfig) {
  return {
    description: 'React Native App Icon',
    prompts: [],
    actions: () => {
      const rootDirectory: string = plop.getPlopfilePath();
      const iosIconDirectory = path.join(
        rootDirectory,
        'ios',
        config.projectName,
        'Images.xcassets',
        `AppIcon.appiconset`
      );
      const iosSplashDirectory = path.join(
        rootDirectory,
        'ios',
        config.projectName,
        'Images.xcassets',
        `LaunchImage.launchimage`
      );
      const androidIconDirectory = path.join(rootDirectory, 'android', 'app', 'src', 'main');
      const androidSplashDirectory = androidIconDirectory;

      // Generate config
      const androidConfig: AndroidConfig = {
        ...config.default,
        ...config.android,
      };
      const iosConfig: IOSConfig = {
        ...config.default,
        ...config.ios,
      };
      const formatFileNames = (fileNames: Array<string>) => fileNames.map((file) => `\n  add ${file} `).join('');
      process.chdir(rootDirectory);

      // Generate tasks
      return [
        // iOS Icon
        ...Maybe.orElse(
          Maybe.map(iosConfig.appIcon, (appIcon) => [
            async () => formatFileNames(await iconGenerator.iOS.generateIcons(appIcon, iosIconDirectory)),
            {
              type: 'add',
              path: path.join(iosIconDirectory, 'Contents.json'),
              force: true,
              templateFile: defaultTemplates.ios['AppIconSetContents.json'],
            },
          ]),
          []
        ),
        // Android Icon
        ...Maybe.orElse(
          Maybe.map(androidConfig.appIcon, (appIcon) => [
            async () => formatFileNames(await iconGenerator.Android.generateIcons(appIcon, androidIconDirectory)),
          ]),
          []
        ),
        // iOS Launch screen
        ...Maybe.orElse(
          Maybe.map(iosConfig.launchImage, (launchImage) => [
            async () => formatFileNames(await iconGenerator.iOS.generateLaunchImage(launchImage, iosSplashDirectory)),
            {
              type: 'add',
              path: path.join(iosSplashDirectory, 'Contents.json'),
              force: true,
              templateFile: defaultTemplates.ios['LaunchImageContents.json'],
            },
            {
              type: 'modify',
              path: path.join('ios', `${config.projectName}.xcodeproj`, 'project.pbxproj'),
              pattern:
                /(\s*)?ASSETCATALOG_COMPILER_APPICON_NAME = AppIcon;(?:(\s*)(ASSETCATALOG_COMPILER_LAUNCHIMAGE_NAME = LaunchImage;)?)*/g,
              template: `$1ASSETCATALOG_COMPILER_APPICON_NAME = AppIcon;$1ASSETCATALOG_COMPILER_LAUNCHIMAGE_NAME = LaunchImage;$1`,
            },
            {
              type: 'modify',
              path: path.join('ios', config.projectName, 'Info.plist'),
              pattern: /<key>UILaunchStoryboardName<\/key>(\s*)<string>LaunchScreen<\/string>/g,
              template: '',
            },
          ]),
          []
        ),
        // Android Launch screen
        ...Maybe.orElse(
          Maybe.map(androidConfig.launchImage, (launchImage) => [
            async () =>
              formatFileNames(await iconGenerator.Android.generateLaunchImage(launchImage, androidSplashDirectory)),
            {
              type: 'add',
              path: `${androidSplashDirectory}/res/drawable/launch_screen_bitmap.xml`,
              force: true,
              templateFile: defaultTemplates.android['launch_screen_bitmap.xml'],
            },
            {
              type: 'add',
              path: `${androidSplashDirectory}/res/values/colors.xml`,
              force: true,
              templateFile: defaultTemplates.android['colors.xml'],
              data: {
                launchScreenBackground: androidConfig.launchImageBackgroundColor,
              },
            },
            {
              type: 'add',
              path: `${androidSplashDirectory}/res/values/styles.xml`,
              force: true,
              templateFile: defaultTemplates.android['styles.xml'],
            },
          ]),
          []
        ),
      ];
    },
  };
}

/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { PlopPlugin, PlopModule } from '@koober/plop-plugin';
import { RNAppIconGenerator, RNAppIconConfig } from './generator';

/**
 * App icon plugin
 *
 * @param plop
 * @param config
 */
export function RNAppIconPlugin(plop: PlopModule, config: RNAppIconConfig) {
  return PlopPlugin.merge({
    generators: {
      'react-native-appicon': RNAppIconGenerator(plop, config),
    },
    helpers: {},
  });
}

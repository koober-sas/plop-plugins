// @ts-check
const { PlopPlugin } = require('@koober/plop-plugin');
const PlopRNIcon = require('@koober/plop-react-native-appicon');

module.exports = (
  /** @type {import('@koober/plop-plugin').PlopModule} */
  plop
) => {
  // apply
  PlopPlugin.apply(
    plop,
    // Combine all plugins
    PlopPlugin.compose(
      // ...other plugins
      PlopRNIcon.default(plop, {
        projectName: 'RNAppExample',
      })
      // ...other plugins
    )
  );
};

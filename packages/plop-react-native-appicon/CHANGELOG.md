# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.4](https://gitlab.com/koober-sas/plop-plugins/compare/@koober/plop-react-native-appicon@1.0.3...@koober/plop-react-native-appicon@1.0.4) (2021-02-24)


### Bug Fixes

* **deps:** update dependency fs-extra to v9 ([79949bb](https://gitlab.com/koober-sas/plop-plugins/commit/79949bb758db4515c74c096fe1464848115354ff))
* **lint:** use conventional naming ([96c1b20](https://gitlab.com/koober-sas/plop-plugins/commit/96c1b2012257652c282ac5aae565b5eed6ca5216))

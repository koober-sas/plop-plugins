// @ts-check
// eslint-disable-next-line import/no-extraneous-dependencies
const { PlopPlugin } = require('@koober/plop-plugin');
const PlopDoc = require('@koober/plop-doc');

module.exports = (
  /** @type {import('@koober/plop-plugin').PlopModule} */
  plop
) => {
  // apply
  PlopPlugin.apply(
    plop,
    // Combine all plugins
    PlopPlugin.compose(
      PlopDoc.default(plop, {
        templates: {},
      }),
      // Custom plugin
      PlopPlugin.merge({
        generators: {
          // Put plugin here
        },
        helpers: {},
      })
    )
  );
};

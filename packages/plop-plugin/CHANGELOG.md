# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.0.4](https://gitlab.com/koober-sas/plop-plugins/compare/@koober/plop-plugin@1.0.3...@koober/plop-plugin@1.0.4) (2021-02-24)


### Bug Fixes

* **deps:** update dependency ts-pipe-compose to ^0.2.0 ([dca08b7](https://gitlab.com/koober-sas/plop-plugins/commit/dca08b7ab100302070ed7c21d7dab8eb561eb043))

// TODO use real typing

export type PlopHelper = any;
export type PlopGenerator = any;
export interface PlopModule {
  getPlopfilePath(): string;
  getGenerator(name: string): PlopGenerator;
  setGenerator(name: string, generator: PlopGenerator): void;
  getHelper(name: string): PlopHelper;
  setHelper(name: string, helper: PlopHelper): void;
}

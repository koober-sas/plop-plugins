/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { PlopPlugin } from './plugin';
import { PlopGenerator } from './plop';
import { createPlopMock } from './testing';

describe('Plugin', () => {
  const createPlop = () => {
    const mock = createPlopMock();
    jest.spyOn(mock, 'setGenerator');
    jest.spyOn(mock, 'setHelper');

    return mock;
  };
  const createGenerator = (name: string): PlopGenerator => ({
    actions: [{ type: `generate_${name}` }],
  });
  const createHelper = (name: string) => (): string => `helper_${name}`;
  const anyGenerator = createGenerator('any');
  const anyHelper = createHelper('any');

  describe('.empty', () => {
    test('should be an empty object', () => {
      expect(PlopPlugin.empty).toEqual({
        generators: {},
        helpers: {},
      });
    });
  });

  describe('.merge', () => {
    test('should merge generators', () => {
      const baseConflict = createGenerator('base');
      const overrideConflict = createGenerator('override');
      expect(
        PlopPlugin.merge({
          generators: {
            conflict: overrideConflict,
            foo: anyGenerator,
          },
          helpers: {},
        })({
          generators: {
            bar: anyGenerator,
            conflict: baseConflict,
          },
          helpers: {},
        })
      ).toEqual({
        generators: {
          bar: anyGenerator,
          conflict: overrideConflict,
          foo: anyGenerator,
        },
        helpers: {},
      });
    });
    test('should merge helpers', () => {
      const baseConflict = createHelper('base');
      const overrideConflict = createHelper('override');
      expect(
        PlopPlugin.merge({
          generators: {},
          helpers: {
            conflict: overrideConflict,
            foo: anyHelper,
          },
        })({
          generators: {},
          helpers: {
            bar: anyHelper,
            conflict: baseConflict,
          },
        })
      ).toEqual({
        generators: {},
        helpers: {
          bar: anyHelper,
          conflict: overrideConflict,
          foo: anyHelper,
        },
      });
    });
    test('should merge other props', () => {
      expect(
        PlopPlugin.merge({
          conflict: 'override',
          foo: true,
          generators: {},
          helpers: {},
        })({
          bar: true,
          conflict: 'base',
          generators: {},
          helpers: {},
        })
      ).toEqual({
        bar: true,
        conflict: 'override',
        foo: true,
        generators: {},
        helpers: {},
      });
    });
  });

  describe('.apply', () => {
    test('should call setGenerator on every generators', () => {
      const plop = createPlop();
      PlopPlugin.apply(plop, {
        generators: {
          foo: anyGenerator,
        },
        helpers: [],
      });

      expect(plop.setGenerator).toHaveBeenCalledWith('foo', anyGenerator);
    });

    test('should call setHelper on every helpers', () => {
      const plop = createPlop();
      PlopPlugin.apply(plop, {
        generators: {},
        helpers: {
          bar: anyHelper,
        },
      });

      expect(plop.setHelper).toHaveBeenCalledWith('bar', anyHelper);
    });

    test('should apply on empty plugin if function is passed', () => {
      const plop = createPlop();
      const plugin = jest.fn(() => ({
        generators: {
          foo: anyGenerator,
        },
        helpers: [],
      }));

      PlopPlugin.apply(plop, plugin);

      expect(plugin).toHaveBeenCalledWith(PlopPlugin.empty);
    });
  });

  describe('.compose', () => {
    test('should return a new composed plugin', () => {
      const plugin1 = PlopPlugin.merge({
        generators: {
          foo: anyGenerator,
        },
        helpers: {},
      });
      const plugin2 = PlopPlugin.merge({
        generators: {
          bar: anyGenerator,
        },
        helpers: {},
      });
      const composedPlugin = PlopPlugin.compose(plugin1, plugin2);
      expect(composedPlugin(PlopPlugin.empty)).toEqual({
        generators: {
          foo: anyGenerator,
          bar: anyGenerator,
        },
        helpers: {},
      });
    });
  });
});

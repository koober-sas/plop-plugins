/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable import/prefer-default-export */
import { PlopModule, PlopGenerator, PlopHelper } from './plop';

export function createPlopMock(): PlopModule {
  const generators: Record<string, PlopGenerator> = {};
  const helpers: Record<string, PlopHelper> = {};

  return {
    getGenerator(name) {
      return generators[name];
    },
    getHelper(name) {
      return helpers[name];
    },
    getPlopfilePath() {
      return 'plopfile/path';
    },
    setGenerator(name, generator) {
      generators[name] = generator;
    },
    setHelper(name, helper) {
      helpers[name] = helper;
    },
  };
}

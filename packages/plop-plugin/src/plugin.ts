/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/no-namespace */
import { compose as tsCompose } from 'ts-pipe-compose';
import { PlopModule, PlopGenerator, PlopHelper } from './plop';

export namespace PlopPlugin {
  export interface Type {
    readonly generators: Record<string, PlopGenerator>;
    readonly helpers: Record<string, PlopHelper>;
  }

  /**
   * Return a new immutable plugin definition
   *
   * @param data
   */
  export function create<T extends Type>({ generators, helpers, ...rest }: T) {
    return Object.freeze({
      generators: Object.freeze(generators),
      helpers: Object.freeze(helpers),
      ...rest,
    });
  }

  /**
   * Return a new functions that will add generators, helpers, etc to its parameter
   *
   * @example
   * // plugin definition
   * const myCustomPlugin1 = PlopPlugin.merge({
   *   generators: {},
   *   helpers: {
   *     helper1: (s) => 'help1(' + s + ')'
   *   },
   * });
   * const myCustomPlugin2 = PlopPlugin.merge({
   *   generators: {},
   *   helpers: {
   *     helper2: (s) => 'help2(' + s + ')'
   *   },
   * });
   *
   * // in plopfile
   * const allPlugins = myCustomPlugin2(myCustomPlugin(PlopPlugin.empty));// merged plugin with helper1 and helper2
   *
   * @param overridePlugin
   * @returns a new plugin
   */
  export function merge<L extends PlopPlugin.Type>({
    helpers: overrideHelpers,
    generators: overrideGenerators,
    ...overrideRest
  }: L) {
    return <R extends PlopPlugin.Type>({ helpers: baseHelpers, generators: baseGenerators, ...baseRest }: R) =>
      create({
        ...baseRest,
        ...overrideRest,
        generators: {
          ...baseGenerators,
          ...overrideGenerators,
        },
        helpers: {
          ...baseHelpers,
          ...overrideHelpers,
        },
      });
  }

  /**
   * Empty plugin
   */
  export const empty = create({
    generators: {},
    helpers: {},
  });

  /**
   * Apply plugin to plop module
   *
   * @example
   *
   * // in plop file
   *
   * module.exports = function (plop) {
   *   const plugin = createMyCustomPlugin(plop, { foo: 'bar' });
   *
   *   PlopPlugin.apply(plop, plugin);// Will actually add generators, helpers, etc on plop
   * };
   *
   *
   * @param plop - plop instance
   * @param pluginOrFactory - plugin or plugin factory to apply
   */
  export function apply(
    plop: PlopModule,
    pluginOrFactory: ((p: PlopPlugin.Type) => PlopPlugin.Type) | PlopPlugin.Type
  ): void {
    const plugin = typeof pluginOrFactory === 'function' ? pluginOrFactory(PlopPlugin.empty) : pluginOrFactory;
    Object.keys(plugin.generators).forEach((key) => {
      plop.setGenerator(key, plugin.generators[key]);
    });
    Object.keys(plugin.helpers).forEach((key) => {
      plop.setHelper(key, plugin.helpers[key]);
    });
  }

  /**
   * Compose a list of plugins to a new plugin
   *
   * @example
   *
   * const composedPlugin = PlopPlugin.compose(
   *   myCustomPlugin1,
   *   myCustomPlugin2
   *   //, ...
   * );
   *
   * @param ...plugins
   * @returns new composed plugin
   */
  export const compose = tsCompose;
}
